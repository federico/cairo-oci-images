#!/bin/sh

zypper update -y
zypper install -y which gcc make \
    automake autoconf libtool git \
    gtk-doc ccache \
    fontconfig-devel freetype2-devel glib2-devel libpng16-compat-devel \
    libpixman-1-0-devel libX11-devel libXext-devel libXrender-devel zlib-devel \
    Mesa-libEGL-devel Mesa-libGL-devel libxcb-devel 
