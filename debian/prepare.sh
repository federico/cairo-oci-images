#!/bin/sh

apt update -yqq && apt upgrade -yqq
apt install -yqq --no-install-recommends \
    pkg-config gcc make \
    automake autoconf libtool \
    gtk-doc-tools git ccache \
    libfontconfig1-dev libfreetype6-dev libglib2.0-dev libpng-dev \
    libpixman-1-dev libx11-dev \
    libxcb1-dev libxext-dev libxrender-dev zlib1g-dev \
    libegl1-mesa-dev libxcb1-dev
