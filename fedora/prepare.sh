#!/bin/sh

dnf upgrade -y
dnf install -y which gcc make \
    automake autoconf libtool \
    gtk-doc git redhat-rpm-config ccache \
    fontconfig-devel freetype-devel glib2-devel libpng-devel \
    pixman-devel libX11-devel libXext-devel libXrender-devel zlib-devel \
    mesa-libEGL-devel mesa-libGL-devel libxcb-devel 
